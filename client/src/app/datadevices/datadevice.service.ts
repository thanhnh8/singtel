import { Injectable } from '@angular/core';
import { DataDevice } from './datadevice';
import { Http, Response } from '@angular/http';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable()
export class DataDeviceService {

    apiRoot = environment.serverUrl;
    constructor(private http: HttpClient) { }

    // get all graphobjs.
    getAllGraphObj() {
      return this.http.get(`${this.apiRoot}/api/list_graph_obj`);
    }

    // get one graphobj.
    getOneGraphObj(params){
      return this.http.get(`${this.apiRoot}/api/list_graph_obj/:id`);
    }

    //

    private handleError(error: any): Promise<any> {
        let errMsg = (error.message) ? error.message :
          error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console
        return Promise.reject(errMsg);
      }
}
