import { Component, OnInit } from '@angular/core';
import { TransactionService } from '../core/transaction.service';
import { HttpClient } from '../../../node_modules/@angular/common/http';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})
export class UploadComponent implements OnInit {
  message = "";
  file = "";

  constructor(
    private http: HttpClient,
    private transactionService: TransactionService
  ) { }

  ngOnInit() {
  }

  onChange(event) {
    this.file = event.srcElement.files;
  }

  uploadFile() {
    console.log("Upload file active");
    // var message = document.getElementById('message');
    this.transactionService.postUploadFile(this.file).subscribe(
      (result: any) => {
        this.message = result.message;
      },
      error => {
        console.log(error);
      }
    );
  }
}
