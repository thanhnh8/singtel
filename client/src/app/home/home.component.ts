import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { TransactionService } from '../core/transaction.service';
import * as _ from 'lodash';
import Web3 from 'web3';
import * as d3 from 'd3';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {
  @ViewChild('txList') txList: ElementRef;
  @ViewChild('dataName') dataName: ElementRef;
  transactions = [];
  tempTransactions = [];
  defaultDatasources = [];
  connectedDatasources = [];

  constructor(private transactionService: TransactionService
  ) {
  }

  ngOnInit() {
    this.transactionService.getListDatasources().subscribe(result => {
      this.defaultDatasources = _.filter(result, function (o) {
        return o.isConnected === false;
      });

      this.connectedDatasources = _.filter(result, function (o) {
        return o.isConnected === true;
      });

      console.log(this.connectedDatasources);
      

      this.drawCanvas();


      // setInterval(() => {
      //   this.transactionService.getTransactions().subscribe(
      //     (result: any) => {
      //       result.forEach(element => {
      //         var web3 = new Web3();
      //         element.input = web3.toAscii(element.input);
      //
      //         let transaction = {};
      //         let data = JSON.parse(element.input) || {};
      //         transaction['txHash'] = element.blockHash;
      //         transaction['type'] = data.Type;
      //         transaction['id'] = element.from;
      //         transaction['timestamp'] = new Date();
      //         transaction['value'] = JSON.stringify(_.omit(data, ['Type', 'Id']));
      //         this.transactions.splice(0, 0, transaction);
      //       });
      //     },
      //     error => {
      //       console.log(error);
      //     }
      //   );
      // }, 1000);
    });
  }

  drawCanvas() {
    let root;
    let sensor;
    let defaultDatasources = this.defaultDatasources;
    let connectedDatasources = this.connectedDatasources;
    let transactionService = this.transactionService;
    let selectedDatasource;

    let showModalCommon = function () {
      document.getElementById("js-modal-common").classList.add('dp-action-show-modal');
    };
    let hideModalCommon = function () {
      document.getElementById("js-modal-common").classList.remove('dp-action-show-modal');
    };

    _.forEach(
      document.querySelectorAll('.js-close-modal, .js-dp-modal-bg'),
      function (item) {
        item.addEventListener('click', function () {
          hideModalCommon();
        });
      }
    );
    document
      .getElementById('js-dp-modal-dialog-common')
      .addEventListener('click', function (event) {
        event.stopPropagation();
      });

    let showModalDetail = function (id) {
      document.getElementById("js-modal-detail").classList.add('dp-action-show-modal');
      let selectedSource = _.find(connectedDatasources, function (o) {
        return o._id === id
      });

      selectedDatasource = selectedSource;

      document.getElementById('dataNameDetail').innerHTML = selectedSource.name;
      document.getElementById('dataTypeDetail').innerHTML = selectedSource.type;
      document.getElementById('dataIdDetail').innerHTML = selectedSource.account;
    };
    let hideModalDetail = function () {
      document.getElementById("js-modal-detail").classList.remove('dp-action-show-modal');
    };

    _.forEach(
      document.querySelectorAll('.js-close-modal-detail, .js-dp-modal-bg'),
      function (item) {
        item.addEventListener('click', function () {
          hideModalDetail();
        });
      }
    );
    document
      .getElementById('js-dp-modal-dialog-detail')
      .addEventListener('click', function (event) {
        event.stopPropagation();
      });
    // "d" parameter is data of current obj

    let height = d3
      .select('#dp-blockchain')
      .node()
      .getBoundingClientRect().height;
    let width = d3
      .select('#dp-blockchain')
      .node()
      .getBoundingClientRect().width;

    let rectangleFirstHeight = d3
      .select('#fake-sensor-default-icon')
      .node()
      .getBoundingClientRect().height; //css height is 109, margin-bottom: 20
    let rectangleFirstWidth = d3
      .select('#fake-sensor-default-icon')
      .node()
      .getBoundingClientRect().width; // is 100%

    let rectangleThirdHeight = d3
      .select('#fake-sensor-animation')
      .node()
      .getBoundingClientRect().height; // css height is 100%
    let rectangleThirdWidth = d3
      .select('#fake-sensor-animation')
      .node()
      .getBoundingClientRect().width; // is 100%

    // draw svg
    let svg = d3
      .select('#dp-blockchain')
      .append('svg')
      .attr({
        width: width,
        height: height
      });

    // draw three rectangle
    let rectangleFirst = svg.append('rect').attr({
      id: 'rectangle-first',
      x: 0,
      y: 0,
      width: width,
      height: rectangleFirstHeight,
      fill: 'rgba(255,255,255,.0)'
    });
    let rectangleSecond = svg.append('rect').attr({
      id: 'rectangle-second',
      x: 0,
      y: rectangleFirstHeight,
      width: width,
      height: 20,
      fill: 'rgba(0,0,0,.0)'
    });
    let rectangleSecondHeight = Number(rectangleSecond.attr('height'))
    let rectangleThird = svg.append('rect').attr({
      id: 'rectangle-third',
      x: 0,
      y: rectangleFirstHeight + 20,
      width: width,
      height: rectangleThirdHeight,
      fill: 'rgba(255,255,255,.0)'
    });

    // Script drag default sensor icon
    let coordinateYCenterOfElement;
    let coordinateXCenterOfElement;
    // icon sensor default r=27
    let coordinateYOfRectangleThird = Number(rectangleFirstHeight) + Number(rectangleSecond.attr('height')) + 27;
    let colorItemDefault = [];
    let dataName = this.dataName;
    let dataId = '';
    let dataPrivate = '';
    let type = '';
    let drag = d3.behavior
      .drag()
      .on('drag', function (d, i) {
        d.x += d3.event.dx;
        d.y += d3.event.dy;

        document.querySelector('#fake-sensor-animation').classList.add('highlight');

        coordinateXCenterOfElement = d.x + Number(d3.select(this).select("g circle").attr('cx'));
        coordinateYCenterOfElement = d.y + Number(d3.select(this).select("g circle").attr('cy'));

        // constrains the nodes to be within a box
        let dxMax = width - 27 - Number(d3.select(this).select("g circle").attr('cx'));
        let dxMin = -(Number(d3.select(this).select("g circle").attr('cx') - 27));
        let dyMax = height - 27 - Number(d3.select(this).select("g circle").attr('cy'));
        let dyMin = -(Number(d3.select(this).select("g circle").attr('cy') - 27));

        d.x = d.x ? (d.x > dxMax ? dxMax : d.x < dxMin ? dxMin : d.x) : 0;
        d.y = d.y ? (d.y > dyMax ? dyMax : d.y < dyMin ? dyMin : d.y) : 0;

        d3.select(this).attr('transform', function (d, i) {
          return 'translate(' + [d.x, d.y] + ')';
        });

      })
      .on('dragstart', function (d) {
        //created clone replace old coordinate
        let clone = d3.select(this).html();
        let textName = d3
          .select(this.parentNode)
          .select('g text')
          .html();
        type = textName;
        let elementMatch = _.find(colorItemDefault, function (obj) {
          return obj.sensorDefaultName === textName;
        });
        //create clone with bg color as before hover
        clone = clone.replace(elementMatch.colorHover, elementMatch.color);

        let dragElement = d3
          .select(this.parentNode)
          .append('g')
          .attr({ style: 'cursor: pointer;' })
          .data([{ x: 0, y: 0 }])
          .attr({ class: 'circle' })
          .attr('transform', function (d) {
            return `translate(${d.x},${d.y})`;
          });
        let cirleTag = dragElement
          .html(clone)
          .on('mouseover', function () {
            d3.select(this)
              .selectAll('g > circle')
              .transition()
              .duration(100)
              .attr('fill', elementMatch.colorHover);
          })
          .on('mouseout', function () {
            d3.select(this)
              .selectAll('g > circle')
              .transition()
              .duration(100)
              .attr('fill', elementMatch.color);
          })
          .call(drag);
      })
      .on('dragend', function (d) {
        let currentSensorName = d3
          .select(this.parentNode)
          .select('g text')
          .html();
        d3.select(this).remove();
        document
          .querySelector('#fake-sensor-animation')
          .classList.remove('highlight');

        if (coordinateYCenterOfElement > coordinateYOfRectangleThird) {
          coordinateYCenterOfElement = 0;
          sensor = _.find(defaultDatasources, function (obj) {
            return obj.name.toUpperCase() === currentSensorName.toUpperCase();
          });

          // generate new account
          var web3 = new Web3();
          let account = web3.eth.accounts.create();

          document.getElementById('dataType').innerHTML = type;
          document.getElementById('dataId').innerHTML = dataId = account.address;
          dataPrivate = account.privateKey;

          showModalCommon();
        }
      });

    document
      .getElementById('btnDelete')
      .addEventListener('click', function() {
        console.log(selectedDatasource);

        transactionService.deleteDatasource(selectedDatasource._id).subscribe(result => {
          transactionService.getListDatasources().subscribe(result => {
            defaultDatasources = _.filter(result, function (o) {
              return o.isConnected === false;
            });

            connectedDatasources = _.filter(result, function (o) {
              return o.isConnected === true;
            }); 
            root = {
              "name": "BC",
              "size": 60000,
              "color": "#C2185B",
              "children": connectedDatasources
            };
            console.log(connectedDatasources);
            
            update();
          });
        });
        
        hideModalDetail();
      });

    document
      .getElementById('js-modal-save')
      .addEventListener('click', function () {
        root.children.push(sensor);
        update();

        let params = {
          name: dataName.nativeElement.value,
          color: sensor.color,
          colorHover: sensor.colorHover,
          img: sensor.img,
          isDefault: false,
          isParent: false,
          isConnected: true,
          type: sensor.type,
          account: dataId,
          private_key: dataPrivate
        };

        transactionService.postDatasource(params).subscribe(
          (result: any) => {
            console.log(result);
          },
          error => {
            console.log(error);
          }
        );
        console.log(JSON.stringify(params));

        // call create api
        // uid: String,
        // name: String,
        // color: String,
        // colorHover: String,
        // img: String,
        // isDefault: Boolean,
        // isParent: Boolean,
        // isConnected: Boolean,
        // type: String,
        // account: String,
        // private_key: String,
        // updated_date: { type: Date, default: Date.now }

        hideModalCommon();
      });

    let r = 27;
    let totalElement = defaultDatasources.length;
    let betweenIcon = 100;

    for (let i = 0; i < totalElement; i++) {
      let data = defaultDatasources[i];
      let colorDefault = { sensorDefaultName: '', color: '', colorHover: '' };
      colorDefault.sensorDefaultName = data.name;
      colorDefault.color = data.color;
      colorDefault.colorHover = data.colorHover;
      colorItemDefault.push(colorDefault);

      let provisionalParent = svg.append('g');

      let provisionalChild = provisionalParent
        .append('g')
        .attr({ style: 'cursor: grab;', class: 'circle' })
        .data([{ x: 0, y: 0 }])
        .on('mouseover', function () {
          d3.select(this)
            .selectAll('g > circle')
            .transition()
            .duration(100)
            .attr('fill', data.colorHover);
        })
        .on('mouseout', function () {
          d3.select(this)
            .selectAll('g > circle')
            .transition()
            .duration(100)
            .attr('fill', data.color);
        })
        .call(drag);

      provisionalChild.append('circle').attr({
        cx: 100 + i * betweenIcon,
        cy: rectangleFirstHeight / 2 - 10,
        r: r,
        fill: data.color,
        stroke: 'rgba(0,0,0,.2)',
        'stroke-width': '0.9'
      });

      provisionalChild.append('image').attr({
        x: 100 + i * betweenIcon - 15,
        y: rectangleFirstHeight / 2 - 10 - 15,
        width: 30,
        height: 30,
        'xlink:href': data.img
      });

      provisionalParent
        .append('text')
        .attr('text-anchor', 'middle')
        .attr('x', i * 100 + 100)
        .attr('y', r * 2 + 40)
        .attr('dy', '0.3rem')
        .attr('fill', 'rgba(0,0,0,.5)')
        .attr({ style: 'font-size: 12px;' })
        .attr({ style: 'font-weight: 600;' })
        .text(data.name);
    }

    //Circle add more is default
    let provisionalParent = svg.append('g');
    let provisionalChild = provisionalParent
      .append('g')
      .attr({ style: 'cursor: pointer;', class: 'circle' })
      .data([{ x: 0, y: 0 }])
      .on('mouseover', function () {
        d3.select(this)
          .selectAll('g > circle')
          .transition()
          .duration(100)
          .attr('fill', 'rgba(192,192,192,0.6)');
      })
      .on('mouseout', function () {
        d3.select(this)
          .selectAll('g > circle')
          .transition()
          .duration(100)
          .attr('fill', 'rgba(192,192,192,0.5)');
      });

    provisionalChild.append('circle').attr({
      cx: rectangleFirstWidth - 100,
      cy: rectangleFirstHeight / 2 - 10,
      r: r,
      fill: 'rgba(192,192,192,0.6)',
      stroke: 'rgba(0,0,0,.2)',
      'stroke-width': '0.9'
    });

    provisionalChild.append('image').attr({
      x: rectangleFirstWidth - 100 - 15,
      y: rectangleFirstHeight / 2 - 10 - 15,
      width: 30,
      height: 30,
      'xlink:href': 'assets/images/sensor/plus.svg'
    });

    provisionalParent
      .append('text')
      .attr('text-anchor', 'middle')
      .attr('x', rectangleFirstWidth - 100)
      .attr('y', r * 2 + 40)
      .attr('dy', '0.3rem')
      .attr('fill', 'rgba(0,0,0,.5)')
      .attr({ style: 'font-size: 12px;' })
      .attr({ style: 'font-weight: 600;' })
      .text('Add more');


    // Draw bg rectangleThird
    let circlesCenterX = width / 2;
    let circlesCenterY =
      Number(rectangleFirstHeight) +
      Number(rectangleSecond.attr('height')) +
      Number(rectangleThirdHeight) / 2;

    let rLarge = 54.5;
    let rMedium = 35;
    svg.append('circle').attr({
      cx: circlesCenterX,
      cy: circlesCenterY,
      r: 2 * rLarge,
      fill: 'rgba(0,0,0,.03)'
    });
    svg.append('circle').attr({
      cx: circlesCenterX,
      cy: circlesCenterY,
      r: 2 * rMedium,
      fill: 'rgba(0,0,0,.04)'
    });

    // let groupBlockChainBg = svg.append('g');
    // let widthEqualHeightImgBlockChain = 90;
    // let widthEqualHeightImgLogo = 40;
    // groupBlockChainBg.append('image').attr({
    //   x:
    //     circlesCenterX -
    //     Math.cos(Math.PI / 4) *
    //       Math.sqrt(2 * Math.pow(widthEqualHeightImgBlockChain / 2, 2)),
    //   y:
    //     circlesCenterY -
    //     Math.cos(Math.PI / 4) *
    //       Math.sqrt(2 * Math.pow(widthEqualHeightImgBlockChain / 2, 2)),
    //   width: widthEqualHeightImgBlockChain,
    //   height: widthEqualHeightImgBlockChain,
    //   'xlink:href': 'assets/images/sensor/blockchain.svg'
    // });
    // groupBlockChainBg.append('image').attr({
    //   x:
    //     circlesCenterX -
    //     Math.cos(Math.PI / 4) *
    //       Math.sqrt(2 * Math.pow(widthEqualHeightImgLogo / 2, 2)),
    //   y:
    //     circlesCenterY -
    //     Math.cos(Math.PI / 4) *
    //       Math.sqrt(2 * Math.pow(widthEqualHeightImgLogo / 2, 2)),
    //   width: widthEqualHeightImgLogo,
    //   height: widthEqualHeightImgLogo,
    //   'xlink:href': 'assets/images/sensor/singtel-logo.png'
    // });

    ///////second
    let force = d3.layout.force()
      .linkDistance(127)
      .charge(-200)
      .gravity(0)
      .size([width, height]);

    let link = svg.selectAll('.link'),
      node = svg.selectAll('.node');

    root = {
      "name": "BC",
      "size": 60000,
      "color": "#C2185B",
      "children": connectedDatasources
    };
    update();

    function update() {
      console.log(root);
      
      let nodes = flatten(root),
        links = d3.layout.tree().links(nodes);

      nodes.forEach(function (d) {
        // if node is root node, initialize coordinate
        if (d.children) {
          d.x = circlesCenterX;
          d.y = circlesCenterY;
          d.fixed = true;
        }
      });

      // Restart the force layout.
      force
        .nodes(nodes)
        .links(links)
        .on('tick', tick)
        .start();

      // Update links.
      link = link.data(links, function (d) {
        return d.target.id;
      });

      link.exit().remove();

      link
        .enter()
        .append('line')
        .attr('class', 'link')
        .attr('style', 'stroke:rgba(0,0,0,.27);stroke-width:0.9');

      // Update nodes.
      node = node.data(nodes, function (d) {
        return d.id;
      });

      node.exit().remove();


      //draw parent node
      let widthEqualHeightImgBlockChain = 90;
      let widthEqualHeightImgLogo = 40;
      let parentNode = node.enter().append("g")
        .filter(function (d) {
          return d.children;
        });
      parentNode.append('image').attr({
        "x": circlesCenterX - Math.cos(Math.PI / 4) * Math.sqrt(2 * Math.pow(widthEqualHeightImgBlockChain / 2, 2)),
        "y": circlesCenterY - Math.cos(Math.PI / 4) * Math.sqrt(2 * Math.pow(widthEqualHeightImgBlockChain / 2, 2)),
        "width": widthEqualHeightImgBlockChain,
        "height": widthEqualHeightImgBlockChain,
        'xlink:href': 'assets/images/sensor/blockchain.svg'
      });
      parentNode.append('image').attr({
        "x": circlesCenterX - Math.cos(Math.PI / 4) * Math.sqrt(2 * Math.pow(widthEqualHeightImgLogo / 2, 2)),
        "y": circlesCenterY - Math.cos(Math.PI / 4) * Math.sqrt(2 * Math.pow(widthEqualHeightImgLogo / 2, 2)),
        "width": widthEqualHeightImgLogo,
        "height": widthEqualHeightImgLogo,
        'xlink:href': 'assets/images/sensor/singtel-logo.png'
      });
      // draw children nodes
      let nodeEnter = node.enter().append("g")
        .attr({
          "class": "node",
          "cursor": "pointer"
        })
        // .on("click", click)
        .filter(function (d) {
          return !d.children;
        })
        .call(force.drag())
        .on('dblclick', function () {
          showModalDetail(d3
            .select(this)
            .select('circle')
            .attr('dataId'));
        })
        .on('click', function () {
          console.log("click")
        });

      nodeEnter.append("circle")
        .attr({
          "dataId": function (d) {
            return d._id
          },
          "r": 27,
          "fill": function (d) {
            return d.color.replace(".7", "1");
          }
        });
      nodeEnter.append('image').attr({
        "x": -15,
        "y": -16,
        width: 30,
        height: 30,
        'xlink:href': function (d) {
          return d.img;
        }
      });

      nodeEnter.append("text")
        .attr({
          "text-anchor": "middle",
          "fill": "rgba(0,0,0,.6)",
          'dy': '.27em',
          "y": 40,
          "font-size": "12px"
        })
        .text(function (d) {
          return d.name;
        });
    }

    function tick() {
      node.attr("transform", function (d) {
        return `translate(${Math.max(27, Math.min(width - 27, d.x))},${Math.max(rectangleFirstHeight + rectangleSecondHeight + 27, Math.min(height - 27, d.y))})`;
      });

      link.attr("x1", function (d) { return d.source.x; })
        .attr("y1", function (d) { return d.source.y; })
        .attr("x2", function (d) { return Math.max(27, Math.min(width - 27, d.target.x)); })
        .attr("y2", function (d) { return Math.max(rectangleFirstHeight + rectangleSecondHeight + 27, Math.min(height - 27, d.target.y)); });
    }

    // Toggle children on click.
    //         function click(d) {
    //             if (d3.event.defaultPrevented) return; // ignore drag
    //             if (d.children) {
    //                 d._children = d.children;
    //                 d.children = null;
    //             } else {
    //                 d.children = d._children;
    //                 d._children = null;
    //             }
    //             update();
    //         }

    // Returns a list of all nodes under the root.
    function flatten(root) {
      var nodes = [],
        i = 0;

      function recurse(node) {
        if (node.children) node.children.forEach(recurse);
        if (node.id) delete node.id;
        if (!node.id) node.id = ++i;
        nodes.push(node);
      }

      recurse(root);
      return nodes;
    }
  }
}
