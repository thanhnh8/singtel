import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './/app-routing.module';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { TransactionService } from './core/transaction.service';
import { HttpClientModule } from '../../node_modules/@angular/common/http';
import { UploadComponent } from './upload/upload.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    UploadComponent
  ],
  imports: [BrowserModule, FormsModule, HttpModule, HttpClientModule, AppRoutingModule],
  providers: [TransactionService],
  bootstrap: [AppComponent]
})
export class AppModule {}
