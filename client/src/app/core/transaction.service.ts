import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { RequestOptions } from '../../../node_modules/@angular/http';

@Injectable()
export class TransactionService {
  apiRoot = environment.serverUrl;

  constructor(private http: HttpClient) {}

  /**
   *
   * @returns {Observable<any>}
   */
  getTransactions() {
    return this.http.get(`${this.apiRoot}/api/transactions`);
  }

  postUploadFile(params) {
    let body = new FormData();
    body.append('originalname', params['0']);
    return this.http.post(`${this.apiRoot}/api/upload`, body);
  }

  public getFakeData() {
    return this.http.get('./assets/data/fakeData.json');
  }

  getListDatasources() {
    return this.http.get(`${this.apiRoot}/datasources/`);
  }

  getDatasource(id) {
    return this.http.get(`${this.apiRoot}/datasources/${id}`);
  }

  postDatasource(params) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    return this.http.post(`${this.apiRoot}/datasources/`, params, httpOptions);
  }

  putDatasource(id) {
    return this.http.get(`${this.apiRoot}/datasources/${id}`);
  }

  deleteDatasource(id) {
    return this.http.delete(`${this.apiRoot}/datasources/${id}`);
  }
}
