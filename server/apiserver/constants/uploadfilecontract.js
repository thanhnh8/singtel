const ContractAddress = '0x545b07663afcf08dd49bf4fea1c63b593b04eb28'
const ABI =
[
	{
		"constant": false,
		"inputs": [
			{
				"name": "_path",
				"type": "string"
			},
			{
				"name": "_hash",
				"type": "string"
			}
		],
		"name": "addFile",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "_hash",
				"type": "string"
			}
		],
		"name": "getFile",
		"outputs": [
			{
				"name": "path",
				"type": "string"
			}
		],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"inputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "constructor"
	}
]

module.exports = {
    'ContractAddress': ContractAddress,
    'ABI': ABI
};
