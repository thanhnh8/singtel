const Node1Url = 'http://35.200.103.213:8501';
const Node2Url = 'http://35.200.246.255:8502';
const Web3 = require("web3");

// Connect to blockchain
const web3 = new Web3();
web3.setProvider(new web3.providers.HttpProvider(Node1Url));
var connected = false;
if(!web3.isConnected()) {
    console.log("Connection to blockchain error");
} else {
    console.log("Connected to blockchain network");
}
module.exports = web3;
