var mongoose = require('mongoose');

var DataSourceSchema = new mongoose.Schema({
  uid: String,
  name: String,
  color: String,
  colorHover: String,
  img: String,
  isDefault: Boolean,
  isParent: Boolean,
  isConnected: Boolean,
  type: String,
  account: String,
  private_key: String,
  updated_date: { type: Date, default: Date.now }
});

module.exports = mongoose.model('DataSource', DataSourceSchema);
