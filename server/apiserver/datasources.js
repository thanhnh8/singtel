var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var DataSource = require('../apiserver/models/DataSource.js');

/* GET ALL DataSource */
router.get('/', function(req, res, next) {
    DataSource.find(function (err, products) {
    if (err) return next(err);
    res.json(products);
  });
});

/* GET SINGLE DataSource BY ID */
router.get('/:id', function(req, res, next) {
    DataSource.findById(req.params.id, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

/* SAVE DataSource */
router.post('/', function(req, res, next) {

    DataSource.create(req.body 
    //Add field isConnected = true if create a new device,
    //and isConnected = false if init database mongo.
    //var strbody = JSON.stringify(req.body);
    //strbody = strbody + ', "isConnected": ' + req.params.isConnected;
    //console.log(JSON.parse(strbody));
    //return JSON.parse(strbody);
  , function (err, post) {
    if (err) return next(err);

    res.json(post);
  });
  
});

/* UPDATE DataSource */
router.put('/:id', function(req, res, next) {
  console.log(req.body);
  DataSource.findByIdAndUpdate(req.params.id, req.body, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

/* DELETE DataSource */
router.delete('/:id', function(req, res, next) {
    DataSource.findByIdAndRemove(req.params.id, req.body, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

module.exports = router;
