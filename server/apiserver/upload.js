const express = require('express');
const router = express.Router();

const multer = require('multer');
const path = require('path');
const fs = require('fs');
const crypto = require('crypto');

// Call smart contract
const Web3 = require("web3");
const contractConstant = require('./constants/uploadfilecontract');

// Upload file
const getStream = require('into-stream');
const inMemoryStorage = multer.memoryStorage();
const uploadStrategy = multer({ storage: inMemoryStorage }).any();
const azureStorage = require('azure-storage');
const AZURE_STORAGE_CONNECTION_STRING = 'DefaultEndpointsProtocol=https;AccountName=singaporetel;AccountKey=fn/qFBoVIWRraGb/Giwn25XKsIX7yjupuxOnL1RMfsAxV2Uli6cI1d5X3nI8urBAkBq1Tu/Eb+lGN/vsNBGinw==';
const blobService = azureStorage.createBlobService(AZURE_STORAGE_CONNECTION_STRING);
const containerName = 'test-container';

// Connect to blockchain
var web3 = require('./constants/blockchainconfig');
var uploadFileContract = web3.eth.contract(contractConstant.ABI);
var uploadFileContractInstance = uploadFileContract.at(contractConstant.ContractAddress);


const getBlobName = originalName => {
    const identifier = Math.random().toString().replace(/0\./, ''); // remove "0." from start of string
    return `${identifier}-${originalName}`;
};

const getFileHash =  function(filestream) {
    const hash = crypto.createHash('sha256');
    const input = filestream;
    let hex = '';
    
        if (input) hex = hash.update(input).digest('hex');
        return hex;
};   

router.post('/api/upload', uploadStrategy, (req, res) => {
    console.log('Upload file:', req.files[0].originalname);
    var encryptedStream = getFileHash(req.files[0].buffer);

    const
        blobName = getBlobName(req.files[0].originalname),
        stream = getStream(encryptedStream),
        streamLength = encryptedStream.length
    ;
    // console.log('encrypt stream: ', encryptedStream);
    // console.log('stream: ', stream);
    // console.log('streamLength: ', streamLength);

    blobService.createBlockBlobFromStream(containerName, blobName, stream, streamLength, err => {
        if(err) {
            handleError(err);
            return;
        }
    });

    // Send data to blockchain
    var objecttosend = {
        "Type": "File",
        "Id": "F001",
        "Value": encryptedStream
    }
    var hexConvertedData = web3.toHex(JSON.stringify(objecttosend));
    var address = '0x09E5CB9974791FB87A0c01dCEFa061084C895D7E';
    var accounts = web3.eth.accounts[0];
    web3.eth.defaultAccount = web3.eth.accounts[0];
    // Send to transaction
    web3.eth.sendTransaction({from: accounts, to: address, value: web3.toWei(0, 'ether'), gasLimit: 21000, gasPrice: 20000000000, data: hexConvertedData});
    
    // Call smart contract
    // uploadFileContractInstance.addFile(JSON.stringify(objecttosend), blobName, (err, result) => {
    //     if (err) {
    //     console.log('Contract call error:',err);
    //     } else {
    //     console.log('Contract result:', result);
    //     }
    // });

    return res.json({status: 200, encryptedKey: hexConvertedData});
});

module.exports = router;