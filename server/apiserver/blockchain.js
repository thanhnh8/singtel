const express = require('express');
const router = express.Router();

var web3 = require('./constants/blockchainconfig');

// CONTACTS API ROUTES BELOW

// Generic error handler used by all endpoints.
function handleError(res, reason, message, code) {
  console.log("ERROR: " + reason);
  res.status(code || 500).json({"error": message});
}

// Get blocks
router.get("/api/blocks", function(req, res) {
  var maxBlocks = 5;
	var blockNum = parseInt(web3.eth.blockNumber, 10);
	if (maxBlocks > blockNum) {
	    maxBlocks = blockNum + 1;
	}

	// get latest 5 blocks
	var blocks = [];
	for (var i = 0; i < maxBlocks; ++i) {
    blocks.push(web3.eth.getBlock(blockNum - i));
  }
  // console.log(blocks);
  res.status(200).json(blocks);
});

// Get transactions
router.get("/api/transactions", function(req, res) {
  var maxBlocks = 3;
  var blockNum = parseInt(web3.eth.blockNumber, 10);

  // console.log('Block number:', blockNum);
	if (maxBlocks > blockNum) {
	    maxBlocks = blockNum + 1;
	}

  var transactions = [];
  var txCount = web3.eth.getBlockTransactionCount(blockNum);

  console.log("Read block: ", blockNum, " - Number of transaction:", txCount);
  if (txCount > 0) {
    // get latest 5 blocks
    for (var transactionIdx = 0; transactionIdx < txCount; transactionIdx++) {
      transactions.push(web3.eth.getTransactionFromBlock(blockNum, transactionIdx));
    };
  }

  //console.log(transactions);
  res.status(200).json(transactions);
});

module.exports = router;