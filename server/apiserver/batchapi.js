var getJSON = require('get-json');
const chalk = require('chalk');

// Connect to blockchain
var web3 = require('./constants/blockchainconfig');
const contractConstant = require('./constants/uploadfilecontract');
var uploadFileContract = web3.eth.contract(contractConstant.ABI);
var uploadFileContractInstance = uploadFileContract.at(contractConstant.ContractAddress);

const APIURL = 'https://api.data.gov.sg/v1/environment/air-temperature';

// Timeout created by setInterval
var intervalLoop = null;

// Main batch
function getDataFromAPI() {
    //console.log(chalk.green("Start call api to get data"));
    // Get data from APIURL = 'https://api.data.gov.sg/v1/environment/air-temperature'
    getJSON(APIURL, function(error, response) {
        if (response.items != undefined) {
            // Send data to blockchain
            var objecttosend = {
                "Type": "API",
                "Id": "A001",
                "Value": response.items
            }
            var hexConvertedData = web3.toHex(JSON.stringify(objecttosend));
            var address = '0x09E5CB9974791FB87A0c01dCEFa061084C895D7E';
            var accounts = web3.eth.accounts[0];
            // if( web3.eth.defaultAccount == undefined) {
            //     web3.eth.defaultAccount = web3.eth.accounts[0];
            // }

            // Send to transaction
            web3.eth.sendTransaction({from: accounts, to: address, value: web3.toWei(0.01, 'ether'), gasLimit: 20000000000000, data: hexConvertedData}, function(err, transactionHash) {
                if (!err) {
                    console.log(chalk.yellow("Sensor API send to blockchain success:", transactionHash));
                } else {
                    console.log(chalk.red("Sensor API error:", err));
                }
            });
            
            // Call smart contract
            // uploadFileContractInstance.addFile(JSON.stringify(objecttosend), blobName, (err, result) => {
            //     if (err) {
            //     console.log('Contract call error:',err);
            //     } else {
            //     console.log('Contract result:', result);
            //     }
            // });
        } else {
            console.log(chalk.red("https://api.data.gov.sg return error"));
        }
    });
}   

// Batch job timer config
intervalLoop = setInterval(getDataFromAPI, 10000);
