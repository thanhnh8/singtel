const express = require("express");
const bodyParser = require("body-parser");
const mongodb = require("mongodb");
const upload = require("./apiserver/upload");
const blockchainapi = require("./apiserver/blockchain");
const sensordataapi = require("./apiserver/batchapi");

const mongoose = require('mongoose');

const readSensorData = require("./simulator/ReadSensorData");
const sensorSimulator = require("./simulator/SensorSimulator");

const datasources = require('./apiserver/datasources');

var path = require('path');

const app = express();

const mongoDB = 'mongodb://singtel:blockchain23@ds141661.mlab.com:41661/singtelblockchain';
mongoose.connect(mongoDB);
// Get Mongoose to use the global promise library
mongoose.Promise = global.Promise;
//Get the default connection
const db = mongoose.connection;
//Bind connection to error event (to get notification of connection errors)
db.on('error', console.error.bind(console, 'MongoDB connection error:'));
db.once('open', () => {
  console.log("Connected to MongoDB server!");
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
// enable CORS
app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
  next();
});
// Create link to Angular build directory.
app.use(express.static(path.join(__dirname, 'public')));

app.use('/datasources', datasources);

app.use(upload);
app.use(blockchainapi);

// Start the server
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`App listening on port ${PORT}`);
  console.log('Press Ctrl+C to quit.');
});
