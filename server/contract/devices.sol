pragma solidity ^0.4.24;

contract devices {
    struct iotDevice {
        string deviceID;
        string organizationID;
        string deviceType;         // in seconds
        string authenMethod;
        string authenToken;
    }

    // contract members
    mapping(string => iotDevice) private iotdevices;
    string[] public existDeviceIDs;
    uint public numDeviceIDs;

    constructor() public {
        numDeviceIDs = 0;
    }

    // loop over this function from 0 to numDeviceIDs to return info on all devices one by one
    // return value are in order: name, use length, location, desired price (by owner), current highest bid, is it up for auction
    function getDeviceInfo (string strDeviceID) public view returns (string, string, string, string, string) {
        return (
        iotdevices[strDeviceID].deviceID,
        iotdevices[strDeviceID].organizationID,
        iotdevices[strDeviceID].deviceType,
        iotdevices[strDeviceID].authenMethod,
        iotdevices[strDeviceID].authenToken
        );
    }

    function registerDevice (
        string strDeviceID, string strOrganizationID, string strDeviceType,
        string strAuthenMethod, string strAuthenToken) public {

        // HungNM39. Check the existing of DeviceID in the system.
        bool isExistDeviceID = false;
        for(uint i = 0; i < numDeviceIDs; ++i) {
            if(keccak256(abi.encodePacked(existDeviceIDs[i])) == keccak256(abi.encodePacked(strDeviceID))) {
                isExistDeviceID = true;
            }
        }

        if (isExistDeviceID) {
            revert();
        }

        iotdevices[strDeviceID] = iotDevice (
            {deviceID: strDeviceID,
            organizationID: strOrganizationID,
            deviceType: strDeviceType,
            authenMethod: strAuthenMethod,
            authenToken: strAuthenToken});

        existDeviceIDs.push(strDeviceID);
        ++numDeviceIDs;
    }

    function deregisterDevice (string strDeviceID) public {

        delete iotdevices[strDeviceID];

        // swap last element with deleted element
        for (uint i = 0; i < numDeviceIDs; ++i) {
            if (keccak256(abi.encodePacked(existDeviceIDs[i])) == keccak256(abi.encodePacked(strDeviceID))) {
                --numDeviceIDs;
                existDeviceIDs[i] = existDeviceIDs[numDeviceIDs];
                delete existDeviceIDs[numDeviceIDs];
                break;
            }
        }
    }

    function updateDevice (
        string strDeviceID, string strOrganizationID, string strDeviceType,
        string strAuthenMethod, string strAuthenToken) public {

        // HungNM39. Check the existing of DeviceID in the system.
        bool isExistDeviceID = false;
        for(uint i = 0; i < numDeviceIDs; ++i) {
            if(keccak256(abi.encodePacked(existDeviceIDs[i])) == keccak256(abi.encodePacked(strDeviceID))) {
                isExistDeviceID = true;
            }
        }

        if (!isExistDeviceID) {
            revert();
        }

        iotDevice memory updatedDevice = iotdevices[strDeviceID];

        updatedDevice.organizationID = strOrganizationID;
        updatedDevice.deviceType = strDeviceType;
        updatedDevice.authenMethod = strAuthenMethod;
        updatedDevice.authenToken = strAuthenToken;

        delete iotdevices[strDeviceID];
        iotdevices[strDeviceID] = updatedDevice;

        // update names array
        for (uint j = 0; j < numDeviceIDs; ++j) {
            if (keccak256(abi.encodePacked(existDeviceIDs[j])) == keccak256(abi.encodePacked(strDeviceID))) {
                existDeviceIDs[j] = strDeviceID;
                break;
            }
        }
    }
}
