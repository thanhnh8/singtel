'use strict';

var connectionString = 'HostName=SensorsIoTHub.azure-devices.net;SharedAccessKeyName=iothubowner;SharedAccessKey=a6Nq2x54Z52xB7Kcz9HG2gJPNnl242WSHW6tCT5cB7M=';
var { EventHubClient, EventPosition } = require('azure-event-hubs');
const chalk = require('chalk');
var Client = require('azure-iothub').Client;
var deviceId = 'MyNodeDevice';
var web3 = require('../apiserver/constants/blockchainconfig');

// Print error message
var printError = function (err) {
  console.log(err.message);
}

// Active device due to alert
var activeDevice = function () {
  console.log(chalk.red('Active device start'));
  // Connect to the service-side endpoint on your IoT hub.
  var client = Client.fromConnectionString(connectionString);

  // Set the direct method name, payload, and timeout values
  var methodParams = {
    methodName: 'SetTelemetryInterval',
    payload: 1, // Number of seconds.
    responseTimeoutInSeconds: 30
  };

  // Call the direct method on your device using the defined parameters.
  client.invokeDeviceMethod(deviceId, methodParams, function (err, result) {
    if (err) {
        console.error('Failed to invoke method \'' + methodParams.methodName + '\': ' + err.message);
    } else {
        console.log(chalk.red('Response from ' + methodParams.methodName + ' on ' + deviceId + ':'));
        console.log(chalk.red(JSON.stringify(result, null, 2)));
    }
  });
}

// Handle data from sensor
var sensorDataHanlder = function (message) {
  //console.log('Telemetry received: ');
  //console.log(JSON.stringify(message.body));
  var sendObject = {
    "Type": "Sensor",
    "Id":  "S001",
    "Temperature": message.body.temperature,
    "Humidity": message.body.humidity
  }
  console.log(chalk.blue(JSON.stringify(sendObject)));

  //console.log('Application properties (set by device): ');
  //console.log(JSON.stringify(message.applicationProperties));
  //console.log('System properties (set by IoT Hub): ');
  //console.log(JSON.stringify(message.annotations));

  if(message.applicationProperties.temperatureAlert == "true") {
    console.log(chalk.red('Temperature alert!'));
    activeDevice();
  }

  // Send data to blockchain
  var hexConvertedData = web3.toHex(JSON.stringify(sendObject));
  var toAddress = '0x09E5CB9974791FB87A0c01dCEFa061084C895D7E';
  var accounts = web3.eth.accounts[0];
  web3.eth.sendTransaction({from: accounts, to: toAddress, value: web3.toWei(0, 'ether'), gasLimit: 21000, gasPrice: 20000000000, data: hexConvertedData}, function(err, transactionHash) {
    if (!err) {
        console.log(chalk.yellow("Sensor API send to blockchain success:", transactionHash));
    } else {
        console.log(chalk.red("Sensor API error:", err));
    }
  });
};

// Connect to the partitions on the IoT Hub's Event Hubs-compatible endpoint.
var ehClient;
EventHubClient.createFromIotHubConnectionString(connectionString).then(function (client) {
  console.log("Successully created the EventHub Client from iothub connection string.");
  ehClient = client;
  return ehClient.getPartitionIds();
}).then(function (ids) {
  //console.log("The partition ids are: ", ids);
  return ids.map(function (id) {
    return ehClient.receive(id, sensorDataHanlder, printError, { eventPosition: EventPosition.fromEnqueuedTime(Date.now()) });
  });
}).catch(printError);