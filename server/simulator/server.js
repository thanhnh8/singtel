const express = require("express");
const bodyParser = require("body-parser");
const readSensorData = require("./ReadSensorData");
const sensorSimulator = require("./SensorSimulator");
var path = require('path');

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

// Enable CORS
app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
  res.header('Access-Control-Allow-Headers', 'X-Requested-With');
  next();
});

// Create link to Angular build directory
app.use(express.static(path.join(__dirname, 'public')));

// Create a database variable outside of the database connection callback to reuse the connection pool in your app.
// var db;
// const DB_USER = "thanhnh8";
// const DB_PWD = "nopass123";
// const DB_URL = `mongodb://${DB_USER}:${DB_PWD}@ds227171.mlab.com:27171/bettingdb`;

// Connect to the database before starting the application server.
// mongodb.MongoClient.connect(process.env.MONGODB_URI || DB_URL, function (err, client) {
//   if (err) {
//     console.log(err);
//     process.exit(1);
//   }

//   // Save database object from the callback for reuse.
//   db = client.db();
//   console.log("Database connection ready");
// });

// Initialize the app.
var server = app.listen(process.env.PORT || 8080, function () {
  var port = server.address().port;
  console.log("App now running on port", port);
});
